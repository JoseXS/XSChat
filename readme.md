## **XSChat**
Es un simple chat sin base de datos creado en NodeJS, Express, Socket.io y Moment

En futuras versiones, se añadirá base de datos en MongoDB

*Basado en el chat de [Victor Robles](https://victorroblesweb.es)*