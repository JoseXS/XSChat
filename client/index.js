var url = 'http://192.168.1.102:6677';
//var url = 'http://xschat.josexs:6677';
var socket = io.connect(url, { 'forceNew': true });

socket.on('messages', function (data) {
    getMessages();
});

function onLoad() {
    if (localStorage.getItem('name')) {
        var divName = document.getElementById('name');
        divName.value = localStorage.getItem('name');
        divName.style.display = 'none';
    }
    var modal = document.getElementById('modal');
    var btn = document.getElementById("btnModal");
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function () {
        modal.style.display = "block";
    }
    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    var formDiv = document.getElementById('form');
    formDiv.clientHeight;
    var messagesDiv = document.getElementById('messages');
    var navBar = document.getElementById('navbar');
    var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 30;
    var style = "height:" + px + 'px';
    messagesDiv.setAttribute("style", style);
}

function addMessage(e) {
    var alertDiv = document.getElementById('alert');
    var messagesDiv = document.getElementById('messages');
    var navBar = document.getElementById('navbar');
    var formDiv = document.getElementById('form');
    if (document.getElementById('name').value == '') {
        alertDiv.innerHTML = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ¡Introduce tu nombre!';
        alertDiv.style.display = 'block';
        var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 40;
        var style = "height:" + px + 'px';
        messagesDiv.setAttribute("style", style);
        setTimeout(function () {
            document.getElementById('alert').style.display = 'none';
            alertDiv.innerHTML = '';
            var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 30;
            var style = "height:" + px + 'px';
            messagesDiv.setAttribute("style", style);
        }, 3000)
        return;
    } else if (document.getElementById('text').value == '') {
        alertDiv.innerHTML = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ¡¡Introduce algo de texto si eso!!';
        alertDiv.style.display = 'block';
        var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 40;
        var style = "height:" + px + 'px';
        messagesDiv.setAttribute("style", style);
        setTimeout(function () {
            document.getElementById('alert').style.display = 'none';
            alertDiv.innerHTML = '';
            var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 25;
            var style = "height:" + px + 'px';
            messagesDiv.setAttribute("style", style);
        }, 3000)
        return;
    } else {
        localStorage.setItem('name', document.getElementById('name').value);
        var message = {
            name: document.getElementById('name').value,
            text: document.getElementById('text').value,
            date: moment().format()
        };
        socket.emit('add-message', message);
        document.getElementById('name').style.display = 'none';
        if (localStorage.getItem('myMessages')) {
            localStorage.setItem('myMessages', Number(localStorage.getItem('myMessages', 1)) + 1);
            document.getElementById('messagesMy').innerText = localStorage.getItem('myMessages');
        } else {
            localStorage.setItem('myMessages', 1);
            document.getElementById('messagesMy').innerText = 1;
        }
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url + '/api/message/add');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify({
            name: document.getElementById('name').value,
            text: document.getElementById('text').value,
            date: moment().format()
        }));
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                document.getElementById('text').value = '';
                var px = window.innerHeight - formDiv.clientHeight - navBar.clientHeight - 30;
                var style = "height:" + px + 'px';
                messagesDiv.setAttribute("style", style);
                return false;
            }
        }
    }
}

function getMessages() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url + '/api/messages', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var messages = JSON.parse(xhr.responseText);
            if (messages.messages.length) {
                if (document.getElementById('messagesTotal')) {
                    document.getElementById('messagesTotal').innerHTML = messages.messages.length;
                    document.getElementById('messagesMy').innerText = localStorage.getItem('myMessages');
                }
            }
            var html = messages.messages.map(function (message, index) {
                var date = moment(message.date).fromNow();
                var div;
                if (parImpar(index) == true) {
                    div =
                        `
                        <div class="message messageRed animated fadeInDown">
                            <strong>${message.name}</strong> <small>(${date})</small>
                            <hr>
                            <p>${message.text}</p>
                            
                        </div>
                    `
                } else {
                    div =
                        `
                        <div class="message messageBlack animated fadeInDown">
                            <strong>${message.name}</strong> <small>(${date})</small>
                            <hr>
                            <p>${message.text}</p>
                            
                        </div>
                    `
                }
                return (div);

            }).join(' ');
            var div_msgs = document.getElementById('messages');
            div_msgs.innerHTML = html;
            div_msgs.scrollTop = div_msgs.scrollHeight;
        }
    }
}

function parImpar(num) {
    if (num % 2 == 0) {
        return true;
    } else {
        return false;
    }
}

function logout() {
    if (localStorage.getItem('name')) {
        localStorage.removeItem('name');
        localStorage.removeItem('myMessages');
        location.reload();
    }
}