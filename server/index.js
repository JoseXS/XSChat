'use strict'

var express = require('express');
var moment = require('moment');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// routes
var messageRoutes = require('./routes/message');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
})
// app.use(express.static('client'));
app.use('/', express.static('client', { redirect: false }));
app.use('/api', messageRoutes);

var messages = [{
    id: 1,
    text: 'Bienvenido al XSChat!',
    name: 'Admin',
    date: moment().format()
}]

io.on('connection', function (socket) {
    console.log('El Cliente con IP ' + socket.handshake.address + ' se ha conectado');
    socket.emit('messages', messages);

    socket.on('add-message', function (data) {
        messages.push(data);
        io.sockets.emit('messages', messages);
    })
});

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/XSChat', { useMongoClient: true })
    .then(() => {
        console.log('Conexion BD OK');
        server.listen(6677, function () {
            console.log('Server OK http://localhost:6677');
        });
    })
    .catch(err => console.log(err));
    module.exports = app;