'use strict'

var mongoose = require('mongoose');
// var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var MessageSchema = Schema({
    name: String,
    text: String,
    date: String
});

// MessageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Message', MessageSchema);