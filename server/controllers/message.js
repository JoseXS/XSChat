'use strict'

//modelos
var Message = require('../models/message');

//acciones
function add(req, res) {
    var message = new Message();
    var params = req.body;
    console.log(params);
    if (params.name) {
        message.name = params.name;
        message.text = params.text;
        message.date = params.date;

        message.save((err, messageStored) => {
            console.log(err);
            console.log(messageStored);
            if (err) {
                res.status(500).send({ message: 'Error en el servidor' });
            } else {
                if (!messageStored) {
                    res.status(404).send({ message: 'No se ha guardado el mensaje' });
                } else {
                    res.status(200).send({ message: messageStored });
                }
            }
        })
    } else {
        res.status(200).send({ message: 'El nombre es obligatorio' });
    }

}

function getAll(req, res) {
    Message.find({}).exec((err, messages) => {
        if (err) {
            res.status(500).send({ message: 'Error en la peticion' })
        } else {
            if (!messages) {
                res.status(404).send({ message: 'No hay mensajes' })
            } else {
                res.status(200).send({ messages })
            }
        }
    })
}

function del(req, res) {
    var articleId = req.params.id;
    Message.findByIdAndRemove(messageId, (err, messageRemove) => {
        if (err) {
            res.status(500).send({ message: 'Error en la peticion' });
        } else {
            if (!messageRemove) {
                res.status(404).send({ message: 'No se ha podido borrar el mensaje (Estaba muy gordo)' });
            } else {
                res.status(200).send({ message: messageRemove });
            }
        }
    })
}

module.exports = {
    add,
    getAll,
    del
}