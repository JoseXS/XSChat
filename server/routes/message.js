'use strict'

var express = require('express');
var MessageController = require('../controllers/message');

var api = express.Router();
var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/isAdmin.js');

// Solo admin
api.post('/message/add', MessageController.add);
// api.delete('/message/:id', [mdAuth.ensureAuth, mdAdmin.isAdmin], MessageController.del);
// Todos
api.get('/messages', MessageController.getAll);

module.exports = api;